from Adafruit_MotorHAT import Adafruit_MotorHAT
import RPi.GPIO as GPIO

class Adafruit_Breakout_DCMotor:
    def __init__(self, pwm, pwm_pin, in1, in2):
        self._pwm = pwm
        self.PWMpin = pwm_pin
        self.IN1pin = in1
        self.IN2pin = in2

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(in1, GPIO.OUT)
        GPIO.setup(in2, GPIO.OUT)

    

    def run(self, command):
        if (command == Adafruit_MotorHAT.FORWARD):
            GPIO.output(self.IN1pin, True)    
            GPIO.output(self.IN2pin, False)
            
        if (command == Adafruit_MotorHAT.BACKWARD):
            GPIO.output(self.IN1pin, False)    
            GPIO.output(self.IN2pin, True)
        if (command == Adafruit_MotorHAT.RELEASE):
            GPIO.output(self.IN1pin, False)    
            GPIO.output(self.IN2pin, False)
            
    def setSpeed(self, speed):
        if (speed < 0):
            speed = 0
        if (speed > 255):
            speed = 255
        self._pwm.setPWM(self.PWMpin, 0, speed*16)

    def cleanup(self):
        GPIO.cleanup()
