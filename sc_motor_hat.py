#!/usr/bin/env python
import sys

from steamcontroller import SteamController, SCButtons
from steamcontroller.events import EventMapper, Pos
from steamcontroller.uinput import Keys
from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor
from Adafruit_Breakout_DCMotor import Adafruit_Breakout_DCMotor
from threading import Timer
from collections import namedtuple
import atexit
from Subfact_ina219 import INA219


MAX_SPEED = 150
MAX_AXIS = pow(2, 15)
MAX_TRIGGER = 255

mh = Adafruit_MotorHAT(addr=0x60)
ina = INA219()
current_hist = {
    "min": 0,
    "max": 0
    }

def turnOffMotors():
    mh.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
    mh.getMotor(2).run(Adafruit_MotorHAT.RELEASE)
    mh.getMotor(3).run(Adafruit_MotorHAT.RELEASE)
    mh.getMotor(4).run(Adafruit_MotorHAT.RELEASE)
    motors[-1].run(Adafruit_MotorHAT.RELEASE)
    motors[-1].cleanup()

atexit.register(turnOffMotors)

motors = map(lambda i: mh.getMotor(i), range(1,5))
motors.append(Adafruit_Breakout_DCMotor(mh._pwm, 15, 18, 17))
motor_timers = [None]
pressed_buttons = []

def button_pressed_callback(evm, btn, pressed):
    print "Button {} was {}.".format(btn, 'pressed' if pressed else 'released')
    if pressed and btn not in pressed_buttons:
        pressed_buttons.append(btn)
    elif not pressed and btn in pressed_buttons:
        pressed_buttons.remove(btn)

    if btn == SCButtons.STEAM and not pressed:
        print "pressing the STEAM button terminates the programm"
        sys.exit()
        
def release_pad_motors():
    motors[1].run(Adafruit_MotorHAT.RELEASE)
    motors[-1].run(Adafruit_MotorHAT.RELEASE)
    motor_timers[0] = None

def touchpad_touch_callback(evm, pad, x, y):
    motor_stop = motors[1] if SCButtons.RGRIP in pressed_buttons else motors[-1]
    motorY = motors[1] if SCButtons.RGRIP not in pressed_buttons else motors[-1]
    print "Tochpad {} was touched @{},{}".format(pad, x, y)
    motor_stop.run(Adafruit_MotorHAT.RELEASE)
    
    if motor_timers[0]:
        motor_timers[0].cancel()
        motor_timers[0] = None

    if y == 0:
        motorY.run(Adafruit_MotorHAT.RELEASE)
    elif y > 0:
        motorY.setSpeed(y*MAX_SPEED/MAX_AXIS)
        motorY.run(Adafruit_MotorHAT.BACKWARD)
    else:
        motorY.setSpeed(-y*MAX_SPEED/MAX_AXIS)
        motorY.run(Adafruit_MotorHAT.FORWARD)
        
    motor_timers[0] = Timer(0.1, release_pad_motors)
    motor_timers[0].start()
    

def stick_axes_callback(evm, x, y):
    motorY1 = motors[2]
    motorY2 = motors[1]
    motorX = motors[3]
    
    if x == 0:
        motorX.run(Adafruit_MotorHAT.RELEASE)
    elif x > 0:
        motorX.setSpeed(x*MAX_SPEED/MAX_AXIS)
        motorX.run(Adafruit_MotorHAT.FORWARD)
    else:
        motorX.setSpeed(-x*MAX_SPEED/MAX_AXIS)
        motorX.run(Adafruit_MotorHAT.BACKWARD)

    if y == 0:
        motorY1.run(Adafruit_MotorHAT.RELEASE)
        motorY2.run(Adafruit_MotorHAT.RELEASE)
    elif y > 0:
        motorY1.setSpeed(y*MAX_SPEED/MAX_AXIS)
        motorY1.run(Adafruit_MotorHAT.FORWARD)
        motorY2.setSpeed(y*MAX_SPEED/MAX_AXIS)
        motorY2.run(Adafruit_MotorHAT.BACKWARD)
    else:
        motorY1.setSpeed(-y*MAX_SPEED/MAX_AXIS)
        motorY1.run(Adafruit_MotorHAT.BACKWARD)
        motorY2.setSpeed(-y*MAX_SPEED/MAX_AXIS)
        motorY2.run(Adafruit_MotorHAT.FORWARD)

    print "Stick Position is {}, {}".format(x, y)


def tigger_axes_callback(evm, pos, value):
    motor = motors[0]
    if value == 0:
        motor.run(Adafruit_MotorHAT.RELEASE)
    elif pos == 0:
        motor.setSpeed(value*MAX_SPEED/MAX_TRIGGER)
        motor.run(Adafruit_MotorHAT.BACKWARD)
    else:
        motor.setSpeed(value*MAX_SPEED/MAX_TRIGGER)
        motor.run(Adafruit_MotorHAT.FORWARD)

    current = ina.getCurrent_mA()
    shuntvoltage = ina.getShuntVoltage_mV()
    busvoltage = ina.getBusVoltage_V()
    loadvoltage = busvoltage + (shuntvoltage / 1000);
    if current > current_hist["max"]:
        current_hist["max"] = current
    if current < current_hist["min"]:
        current_hist["min"] = current
    print "Trigger axes %s has value %s. Current %d [%d/%d] mA. Voltage %s-%s-%s" % (pos, value, current, current_hist["min"], current_hist["max"], shuntvoltage, busvoltage, loadvoltage)

def evminit():
    evm = EventMapper()
    evm.setButtonCallback(SCButtons.STEAM, button_pressed_callback)
    evm.setButtonCallback(SCButtons.RGRIP, button_pressed_callback)
    evm.setPadButtonCallback(Pos.RIGHT, touchpad_touch_callback)
    evm.setStickAxesCallback(stick_axes_callback)
    evm.setTrigAxesCallback(Pos.RIGHT, tigger_axes_callback)
    evm.setTrigAxesCallback(Pos.LEFT, tigger_axes_callback)
    return evm


if __name__ == '__main__':
    evm = evminit()
    sc = SteamController(callback=evm.process)
    sc.run()
